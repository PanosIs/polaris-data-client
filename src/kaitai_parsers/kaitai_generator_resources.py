SCRIPT_IGNORE_LIST = ['BytesIO', 'Enum', 'KaitaiStream', 'KaitaiStruct', '__builtins__', '__cached__', '__doc__',
                      '__file__', '__loader__', '__name__', '__package__', '__spec__', 'ks_version', 'parse_version']
DONTFOLLOW = ['close', 'from_bytes', 'from_file', 'from_io']
INFORMATION_TYPES = [int, str, bool, bytes]

KAITAI_COMPILER_DIR = '/kaitai_compiler/bin/'
KAITAI_SCRIPT_DIR = '/kaitai_script/'