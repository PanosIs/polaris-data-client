import requests
from bs4 import BeautifulSoup

from src.networking.satnogs_resources import SATNOGS_BASE, SATNOGS_SAT


def get_kaitai_struct(norad_id : int):
    page = requests.get(SATNOGS_BASE + SATNOGS_SAT + str(norad_id))
    soup = BeautifulSoup(page.content, 'html.parser')

    try:
        return soup.find('div', attrs={'class' : 'kaitai-struct'}).find('pre').text
    except:
        return "None"