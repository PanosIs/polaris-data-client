
SATNOGS_BASE = 'https://db.satnogs.org/'
SATNOGS_SAT = 'satellite/'

SATNOGS_API = 'api/'
SATNOGS_API_SATELLITE = 'satellites/'
SATNOGS_API_TELEMETRY = 'telemetry/'