import json

import requests

from src.database.database_api import *
from src.networking.satnogs_resources import *
from src.networking.satnogs_scraper import get_kaitai_struct


def load_satellites():
    response = requests.get(SATNOGS_BASE + SATNOGS_API + SATNOGS_API_SATELLITE)
    json_file = json.loads(response.content.decode())
    for satellite in json_file:
        print(satellite)
        insert_satellite(satellite, get_kaitai_struct(satellite['norad_cat_id']))

def load_satellite_by_id(norad_cat_id : int):
    response = requests.get(SATNOGS_BASE + SATNOGS_API + SATNOGS_API_SATELLITE + str(norad_cat_id))
    satellite = json.loads(response.content.decode())
    insert_satellite(satellite, get_kaitai_struct(satellite['norad_cat_id']))

def load_telemetry_page(norad_cat_id : int, page : int, manager : TelemetryManager):
    response = requests.get('https://db.satnogs.org/api/telemetry/?page={}&satellite={}'.format(page, norad_cat_id))
    next_page_exists = False
    print("Response code: {}".format(response.status_code))
    if(response.status_code == 200):
        json_page = json.loads(response.content.decode())

        for telemetry_entry in json_page:
            manager.insert_telemetry(telemetry_entry)

        set_satellite_last_parsed_page(norad_cat_id, page)

        if ('Link' in response.headers.keys()):
            if ('next' in response.headers['Link']):
                next_page_exists = True
    else:
        pass
    return next_page_exists

def load_telemetry_for_satellite(norad_cat_id : int, start_page : int = None):
    if(not get_satellite(norad_cat_id)):
        return

    if(start_page == None):
        start_page = get_satellite_last_parsed_page(norad_cat_id)

    manager = TelemetryManager(norad_cat_id)
    page = start_page

    while True:
        print("Loading page: {}".format(page))
        next = load_telemetry_page(norad_cat_id, page, manager)
        if(next):
            page += 1
        else:
            break