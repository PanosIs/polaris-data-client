from src.database import database_api
from src.networking.satnogs_db_requests import *
from flask import Flask
from flask import request, abort, make_response, jsonify
from threading import Thread
from datetime import datetime

app = Flask(__name__)

#
# FUNCTIONING ENDPOINTS
#

@app.route('/satellites', methods=['POST'])
def request_all_satellites():
    try:
        thread = Thread(target=load_satellites)
        thread.start()
    except:
        abort(500)
    return jsonify("Request accepted")

# Request that the server acquires or updates the data for a specific satellite
# TODO: Add check and don't update satellite if within a certain time period
@app.route('/satellites/<int:norad_cat_id>', methods=['POST'])
def request_satellite(norad_cat_id):
    try:
        thread = Thread(target=load_satellite_by_id, args=[norad_cat_id,])
        thread.start()
    except:
        abort(500)
    return jsonify("Request accepted")

# Request a list of satellites along with their status and last update
@app.route('/satellites', methods=['GET'])
def get_satellite_list():
    return jsonify(database_api.get_satellites(raw = True))

# Request a specific satellite along with all information pertaining to it
@app.route('/satellites/<int:norad_cat_id>', methods=['GET'])
def get_satellite(norad_cat_id):
    return jsonify(database_api.get_satellite(norad_cat_id, raw = True))

# Request that the server acquires or updates its telemetry for a specific satellite
@app.route('/telemetry/<int:norad_cat_id>', methods=['POST'])
def request_telemetry(norad_cat_id):
    try:
        thread = Thread(target=load_telemetry_for_satellite, args=[norad_cat_id,])
        thread.start()
    except:
        abort(500)
    return jsonify("Request accepted")

# Request all telemetry items for a specific satellite
@app.route('/telemetry/<int:norad_cat_id>', methods=['GET'])
def get_telemetry(norad_cat_id):
    params = request.args
    date_from, date_to = None, None
    if('date_start' in params.keys()):
        date_from = datetime.strptime(params['date_start'], "%Y-%m-%dT%H:%M:%SZ")
    if('date_end' in params.keys()):
        date_to = datetime.strptime(params['date_end'], "%Y-%m-%dT%H:%M:%SZ")
    return jsonify(database_api.get_telemetry(norad_cat_id, date_from=date_from, date_to=date_to, raw = True))

#
# NONFUNCTIONING ENDPOINTS
#

# Request that the server creates a dataset with the given parameters
@app.route('/datasets', methods=['POST'])
def request_dataset():
    abort(501)

# Request a list of datasets along with their status and details
@app.route('/datasets', methods=['GET'])
def get_dataset_list():
    abort(501)

# Request a specific dataset from the server
@app.route('/datasets/<int:dataset_id>', methods=['GET'])
def get_dataset(dataset_id):
    abort(501)

if __name__ == '__main__':
    app.run()