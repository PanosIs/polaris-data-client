from mongoengine import connect

from src.database.database_model import Satellite, Telemetry
from src.kaitai_parsers.kaitai_parse_generic import AutoParser, generate_kaitai_parser_script
from src.utils.validate_kaitai import valid_kaitai

connect("Wheatly")

class TelemetryManager():
    def __init__(self, norad_cat_id : int):
        self.norad_cat_id = norad_cat_id
        sat = Satellite.objects(norad_cat_id = norad_cat_id).first()
        if(sat.has_kaitai):
            self.parser = AutoParser(sat.kaitai_script_name)
        else:
            self.parser = None

    def insert_telemetry(self, json_telemetry : dict, parse : bool = True):
        telemetry = Telemetry(**json_telemetry)
        try:
            try:
                if(parse and self.parser != None):
                    telemetry.information = self.parser.get_info_from_frame(telemetry.frame)
            except:
                telemetry.information = None
                print("Frame parsing error.")
            telemetry.save()
        except:
            pass

def get_telemetry(norad_cat_id, date_from = None, date_to = None, raw = False):
    query_set = Telemetry.objects.exclude('id').filter(norad_cat_id = norad_cat_id)
    if(date_from != None):
        query_set = query_set.filter(timestamp__gte = date_from)
    if(date_to != None):
        query_set = query_set.filter(timestamp__lte = date_to)
    if(raw):
        query_set = query_set.as_pymongo()
    return list(query_set)

def insert_satellite(json : dict, kaitai_struct : str):

    script = None
    if(valid_kaitai(kaitai_struct)):
        script = generate_kaitai_parser_script(json['norad_cat_id'], kaitai_struct)

    if(script != None):
        json['has_kaitai'] = True
        json['kaitai_struct'] = kaitai_struct
        json['kaitai_script_name'] = generate_kaitai_parser_script(json['norad_cat_id'], kaitai_struct)
    else:
        json['has_kaitai'] = False

    sat = Satellite(**json)
    sat.last_loaded_telemetry_page = 1
    sat.save()

def get_satellite(norad_cat_id : int, raw = False) -> Satellite:
    if(not raw):
        return Satellite.objects(norad_cat_id = norad_cat_id).first()
    else:
        return Satellite.objects(norad_cat_id=norad_cat_id).as_pymongo().first()

def get_satellite_ids(with_kaitai = None):
    if(with_kaitai == None):
        return [s.id for s in Satellite.objects()]
    else:
        return [s.id for s in Satellite.objects(has_kaitai = with_kaitai)]

def get_satellites(raw = False):
    if(not raw):
        return list(Satellite.objects)
    else:
        return list(Satellite.objects.as_pymongo())

def set_satellite_last_parsed_page(norad_cat_id : int, page : int):
    sat = get_satellite(norad_cat_id)
    sat.last_loaded_telemetry_page = page
    sat.save()

def get_satellite_last_parsed_page(norad_cat_id : int):
    sat = get_satellite(norad_cat_id)
    return sat.last_loaded_telemetry_page