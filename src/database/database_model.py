from mongoengine import *

class Dataset(Document):
    # Dataset status can be: In queue, Pending, Available
    status = StringField(required=True)

    # Dataset content can be: None, Partial, Complete
    content = StringField(required=True)

    satellites = ListField(IntField(), required=True)
    date_from = DateTimeField(required=True)
    date_to = DateTimeField(required=True)



class Telemetry(Document):
    norad_cat_id = IntField(required=True)
    frame = StringField(required=True)
    timestamp = DateTimeField(required=True)

    information = DictField()

    transmitter = StringField()
    source = StringField()
    schema = StringField()
    decoded = StringField()
    observer = StringField()

    meta = {
        'indexes': [
            {
                'fields' : ['norad_cat_id', 'timestamp'],
                'unique' : True
            }
        ]
    }

class Satellite(Document):
    norad_cat_id = IntField(primary_key=True, required=True)
    name = StringField(required=True)
    status = StringField(required=True)

    has_kaitai = BooleanField(required=True)
    kaitai_struct = StringField()
    kaitai_script_name = StringField()

    names = StringField()
    image = StringField()
    decayed = StringField()

    last_loaded_telemetry_page = IntField()