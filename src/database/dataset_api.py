from typing import List

import pandas

def format_telemetry(telemetry):
    temp = telemetry['information']
    temp['timestamp'] = telemetry['timestamp']
    temp['norad_cat_id'] = telemetry['norad_cat_id']
    return temp

def get_dataframe_from_raw(telemetry_list : List[dict], decode_only = True):
    if(decode_only):
        return pandas.DataFrame([format_telemetry(tel) for tel in telemetry_list])
    else:
        return pandas.DataFrame(pandas.io.json.json_normalize(telemetry_list))