# Polaris Data Client

A client that interacts with the SatNOGS API and makes data available in the form of datasets in pickled files. 

This is very much a work in progress. 

# How to use an incomplete app

You will need to have the dependencies in dependencies.txt, which you can install with pip, as well as mongoDB installed on your system. 

You can either run the application.py script to create a Flask hook in localhost and use the endpoints /satellite/<norad_cat_id> and /telemetry/<norad_cat_id> to load data into the database, or use a custom script to call the functions in satnogs_db_requests.py. To generate datasets from the database use the dataset_api.py script under the src/database folder. 